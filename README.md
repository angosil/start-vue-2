# Start Vue 2

Start Vue is a simple project in wish I try to put every necessary to start a single page Vue2 project. I start with
Docker to do that I prepare a Dockerfile and docker compose and star with some file structure. This structure is likely
to change throughout development.

## Initial structure

1. `app` The application content. this directory contains the necessary file to create our application.
2. `app/Dockerfile` The initial Dockerfile to star using node and Vue2.
3. `script` Some important script to facilitate the development of the application.
4. `script/hard_clear.sh` stop and remove all container, images and volumes in local.
5. `script/restart.dev.sh` down, start, and exec bash the docker using docker-compose.
6. `docker-compose.yaml` the start docker-compose file for the application.

## Start with Vue2.

I follow the official documentation [Creating a Project](https://cli.vuejs.org/guide/creating-a-project.html). I try to
create this new project with [Vue CLI](https://cli.vuejs.org/).

To create the project with vue I run the script `script/restart.dev.sh` to up and enter the container.

```bash
$ ./scripts/restart.dev.sh
...
root@7789c8581467:~/app#
```

Inside the container we can check the Vue CLI version. At the time of this tutorial the version was 5.0.4.

```bash
root@39c202d6c95e:~/app# vue -V
@vue/cli 5.0.4
```

Create the project with Vue cli, our project directory contain files for vue project.

```bash
root@39c202d6c95e:~/app# vue create project
```

Then select the option `Manually select features`. We have the possibility to start a wit some functionalities added,
apart from Babel - ESLint which is the default basic option.

```bash
Vue CLI v5.0.4
? Please pick a preset:
  Default ([Vue 3] babel, eslint)
  Default ([Vue 2] babel, eslint)
❯ Manually select features
```

Nex we have to chose som options. First Babel, Router and Linter.

```bash
Vue CLI v5.0.4
┌─────────────────────────────────────────┐
│                                         │
│   New version available 5.0.4 → 5.0.5   │
│    Run npm i -g @vue/cli to update!     │
│                                         │
└─────────────────────────────────────────┘

? Please pick a preset: Manually select features
? Check the features needed for your project: (Press <space> to select, <a> to toggle all, <i> to invert selection, and <enter> to proceed)
 ◉ Babel
 ◯ TypeScript
 ◯ Progressive Web App (PWA) Support
 ◉ Router
❯◉ Vuex
 ◯ CSS Pre-processors
 ◉ Linter / Formatter
 ◯ Unit Testing
 ◯ E2E Testing
```

Second selection is like.

```bash
Vue CLI v5.0.4
┌─────────────────────────────────────────┐
│                                         │
│   New version available 5.0.4 → 5.0.5   │
│    Run npm i -g @vue/cli to update!     │
│                                         │
└─────────────────────────────────────────┘

? Please pick a preset: Manually select features
? Check the features needed for your project: Babel, Router, Vuex, Linter
? Choose a version of Vue.js that you want to start the project with 2.x
? Use history mode for router? (Requires proper server setup for index fallback in production) Yes
? Pick a linter / formatter config: Basic
? Pick additional lint features: Lint on save
? Where do you prefer placing config for Babel, ESLint, etc.? In dedicated config files
? Save this as a preset for future projects? No
```

Finally, we have the confirmation from vue. I try to put some references to understand the package selection in the
first steep. [Babel](https://babeljs.io/docs/en/), [Vue Router](https://router.vuejs.org/introduction.html)
, [Vuex](https://vuex.vuejs.org/) and [Linter](https://vue-loader.vuejs.org/guide/linting.html)

```bash
Vue CLI v5.0.4
✨  Creating project in /home/node/app/project.
⚙️  Installing CLI plugins. This might take a while...


added 841 packages in 21m
npm notice
npm notice New minor version of npm available! 8.5.5 -> 8.12.2
npm notice Changelog: https://github.com/npm/cli/releases/tag/v8.12.2
npm notice Run npm install -g npm@8.12.2 to update!
npm notice
🚀  Invoking generators...
📦  Installing additional dependencies...


added 85 packages in 17s
⚓  Running completion hooks...

📄  Generating README.md...

🎉  Successfully created project project.
👉  Get started with the following commands:

 $ cd project
 $ npm run serve
```

Move to the `project` directory and run the `npm run server` command.

```bash
root@39c202d6c95e:~/app# cd project/
root@39c202d6c95e:~/app/project# npm run serve

> project@0.1.0 serve
> vue-cli-service serve

 INFO  Starting development server...
```

Finally, we have the dev server running inside our docker environment.

```bash
 DONE  Compiled successfully in 51060ms                                                                              9:09:56 AM

  App running at:
  - Local:   http://localhost:8080/

  It seems you are running Vue CLI inside a container.
  Access the dev server via http://localhost:<your container's external mapped port>/

  Note that the development build is not optimized.
  To create a production build, run npm run build.
```

Go to the [http://localhost:8080/](http://localhost:8080/) and voilá we have our dev environment running.

![vue dev page](doc-images/first.page.png)

and at this point I make a git commit to save all in the repo.

```bash
$ git add .
$ git commit -m "added vue and project"
...
$ git push origin main
```

## Add environment var to our project.

At this point is convenient some explanation about what we have and the structure. Whe I launch the command to create
the project Vue create a basic structure.

```bash
$ tree -L 1
.
├── README.md
├── babel.config.js
├── jsconfig.json
├── node_modules
├── package-lock.json
├── package.json
├── public
├── src
└── vue.config.js
```

I only focus in the src directory, this directory contains our source code. I try to introduce some changes in those
files to load data from env vars, I
follow [HTML and Static Assets](https://cli.vuejs.org/guide/html-and-static-assets.html). First I try to add environment
var to our docker-compose file and use in the vue project. This allows to us add some environment variables to our
images like the api ent point for the backend. I follow the documentation
in [Modes and Environment Variables](https://cli.vuejs.org/guide/mode-and-env.html). so, first we add the env var to the
docker-compose file.

`/docker-compose.yaml`

```yaml
version: '3.8'

services:
  app:
    build:
      context: app
      dockerfile: Dockerfile
    restart: always
    environment: # New
      - VUE_APP_TITLE=My Vue App # New
    stdin_open: true
    tty: true
    volumes:
      - ./app/:/home/node/app
    ports:
      - "8080:8080"
```

Inside the `project/src` directory.

```bash
$ tree src
src
├── App.vue
├── assets
│   └── logo.png
├── components
│   └── HelloWorld.vue
├── main.js
├── router
│   └── index.js
├── store
│   └── index.js
└── views
    ├── AboutView.vue
    └── HomeView.vue

5 directories, 8 files
```

The first modification to introduce is in `project/src/views/HomeView.vue` component in the template.

```vue

<template>
  <div id="app">
    <img alt="Vue logo" src="./assets/logo.png">
    <HelloWorld :msg="msg"/><!-- New -->
  </div>
</template>

....
```

and in the same component in the script block. Add data property to store the env var value.

```vue

<script>
import HelloWorld from './components/HelloWorld.vue'

export default {
  name: 'App',
  components: {
    HelloWorld
  },
  data() { // new
    return {
      msg: process.env.VUE_APP_TITLE
    }
  }
}
</script>
```

This is all, with those changes we can test the possibility to set configuration vars to aur projects. Star the dev
server inside the container.

```bash
$ ./scripts/restart.dev.sh
```

and

```bash
root@74bfef4b620c:~/app# cd project/
root@74bfef4b620c:~/app/project# npm run serve
```

and we have.

![vue dev page](doc-images/home.page.title.png)

