echo "Down docker containers"
docker-compose down --rmi all -v

echo "Starting docker containers"
docker-compose up --build -d


echo "Exec in container"
docker-compose exec app bash
#echo "Waiting for app service ..."
#while ! curl http://127.0.0.1:8095 -m1 -o/dev/null -s ; do
#  echo "Waiting ..."
#  sleep 1
#done

echo "App is up"